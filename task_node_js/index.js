const http = require('http');

const host = '127.0.0.1';
const port = 4000;
let users = [
	{ id: '1', name: 'user1' },
	{ id: '2', name: 'user2' },
	{ id: '3', name: 'user3' },
];

const server = http.createServer(async (req, res) => {
	switch (req.method) {
		case 'GET':
			res.statusCode = 200;
			res.setHeader('ContentType', 'application/json');
			res.end(JSON.stringify(users));
			break;
		case 'POST': //schema: {id:5,name:user5}
			asyncIterators(req).then((data) => users.push(JSON.parse(data)));
			res.end('Данные успешно добавлены!');

			break;
		case 'PUT': // shema: {users:[{id:5,name:user5}]}
			const data = await asyncIterators(req);
			const newData = JSON.parse(data);
			res.end('Данные успешно изменены!');
	}
});

async function asyncIterators(req) {
	const buffers = [];

	for await (const chunk of req) {
		buffers.push(chunk);
	}

	return Buffer.concat(buffers).toString();
}

server.listen(port, host, () => {
	console.log(`Server works on ${host}:${port}`);
});
