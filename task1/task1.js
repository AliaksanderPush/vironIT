/*
Имеется массив простых чисел: numbers = [2, 3, 5, 7, 11, 13, 17, 19]. Используя метод reduce(), создаем функцию currentSums (numbers)
, которая возвращает новый массив из такого же числа элементов, в котором на каждой позиции будет находиться сумма элементов массива numbers до этой позиции включительно.
Т.е. для numbers = [2, 3, 5, 7, 11, 13, 17] мы должны увидеть, вызвав currentSums
*/

const numbers = [2, 3, 5, 7, 11, 13, 17];
function currentSums(arr) {
  let array = [];
  arr.reduce((acc, item, i) => {
    return (array[i] = acc + item);
  }, 0);
  return array;
}

console.log(currentSums(numbers));

/*
На входе имеем строку. Напишите функцию, которая получает массив из первых букв слов этой строки. При написании данного кода, позаимствуйте у объекта Array метод filter.
Например:
var str = "Каждый охотник желает знать, где сидит фазан."; 
console.log(newArray);  // [К,о,ж,з,г,с,ф]
*/

const str = "Каждый охотник желает знать, где сидит фазан.";

function changeString(str) {
  const arr = str.split(" ");
  let newArray = [];
  arr.filter((item, i) => (newArray[i] = item[0]));
  return newArray;
}

console.log(changeString(str));

/*
Создать функцию changeArray, которая будет менять местами половины массивов, т.е. если количество элементов четное, то вторая половина становится сначала массива, а первая на место второй; если количество элементов нечетное, тогда элемент массива, который является серединой остается на месте, а половины меняются местами.

Например:
changeArray([ 1, 2, 3, 4, 5 ]); //[ 4, 5, 3, 1, 2 ]
changeArray([ 1, 2 ]); //[ 2, 1 ] 
changeArray([ 1, 2, 3, 4, 5, 6, 7, 8]);  //[ 5, 6, 7, 8, 1, 2, 3, 4 ]
*/

function changeArray(arr) {
  if (arr.length < 1) return arr;
  const pivotIndex = Math.floor(arr.length / 2);
  const left = arr.slice(0, pivotIndex);
  if (arr.length % 2 !== 0) {
    const pivot = arr[pivotIndex];
    const righth = arr.slice(pivotIndex + 1);
    return [...righth, pivot, ...left];
  } else {
    const righth = arr.slice(pivotIndex);
    return [...righth, ...left];
  }
}

console.log(changeArray([1, 2, 3, 4, 5]));
console.log(changeArray([1, 2, 3, 4]));
console.log(changeArray([1, 2]));
console.log(changeArray([1, 2, 3, 4, 5, 6, 7, 8]));
