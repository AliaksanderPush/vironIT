(function () {
  document.querySelector(".btn1").addEventListener("click", drinckInfo);
  document.querySelector(".btn2").addEventListener("click", getDrinckInfo);
  document.querySelector(".btn3").addEventListener("click", removeInfoDrinck);
  document.querySelector(".btn4").addEventListener("click", getDrinckInfoAll);

  const coctailsStorage = new HashStorage();
  window.addEventListener("load", start);

  function start() {
    coctailsStorage.addValue("Маргарита", {
      alcoholic: "да",
      recept: `
             Наполни стакан кубиками льда доверху,
             затем налей кофейный ликер 25 мл, 
             водку 50 мл и размешай коктейльной ложкой`,
      prop: `
             Водка Finlandia 50мл
             Кофейный ликер 25мл
             Лед в кубиках 120 г
    `,
    });
    coctailsStorage.addValue("Пеликан", {
      alcoholic: "да",
      recept: `
            Гренадин Monin 10мл
            Клубничный сироп Monin 10мл
            Персиковый сок 150мл
            Лимонный сок 15мл
            Банан 110г
            Клубника 50г
            Дробленый лед 60г
               `,
      prop: `
             Положи в блендер очищенную и нарезанную
             половинку банана и клубнику 2 ягоды.
             Налей лимонный сок 15 мл, гренадин 10 мл,
             клубничный сироп 10 мл и персиковый сок 
             150 мл. Добавь в блендер совок дробленого
             льда и взбей. Перелей в хайбол. Укрась
             кружком банана и половинкой клубники
             на коктейльной шпажке.
      `,
    });
  }
  coctailsStorage.addValue("Колалайт", {
    alcoholic: "да",
    recept: `
             Наполни стакан колой,
             затем налей лайм 25 мл,
             кубик льда, 
             водку 50 мл и размешай коктейльной ложкой`,
    prop: `
              Водка Absolute 50мл
              Лайм 25мл
              Лед в кубиках 120 г
  `,
  });

  function drinckInfo() {
    let drinck = prompt("Введите название напитка:", "");
    while (!drinck) {
      drinck = prompt(
        `
      Вы не ввели данные!!!! 
      Введите название напитка:`,
        ""
      );
    }
    let alc = confirm("Он алкогольный или нет?", "");

    let proportions = prompt(
      "Какие ингредиенты необходимы и в каких пропорциях?",
      ""
    );
    while (!proportions) {
      proportions = prompt(
        `
        Вы не написали ингридиенты!!!
        Напишите ингридиенты`,
        ""
      );
    }

    let rec = prompt("Напишите рецепт его приготовления", "");
    while (!rec) {
      rec = prompt(
        `
        Вы не написали рецепт!!!
        Напишите рецепт его приготовления`,
        ""
      );
    }

    coctailsStorage.addValue(drinck, {
      alcoholic: alc,
      recept: rec,
      prop: proportions,
    });
    alert("Данные внесены успешно!");
  }

  function getDrinckInfo() {
    let getInfo = prompt("Введите напиток рецепт которого хотите узнать", "");
    if (!getInfo) {
      getInfo = prompt(
        `
       Вы не ввели данные!!!
       Введите напиток рецепт которого хотите узнать`,
        ""
      );
    }

    let getInfoDrinck = coctailsStorage.getValue(getInfo);

    if (getInfoDrinck) {
      const { alcoholic, recept, prop } = getInfoDrinck;
      alert(`
             НАПИТОК: ${getInfo}
             АЛКОГОЛЬНЫЙ: ${alcoholic ? "да" : "нет"}
             ИНГРИДИЕНТЫ И ПРОПОРЦИИ: ${prop}
             РЕЦЕПТ ПРИГОТОВЛЕНИЯ: ${recept}`);
    } else {
      alert("Информации о таком напитке нет(((");
    }
  }

  function removeInfoDrinck() {
    let removeInfoDrinck = prompt("Введите напиток который хотите удалить", "");
    if (!removeInfoDrinck) {
      prompt(
        `
         Вы не ввели данные!!!
         Введите напиток который хотите удалить`,
        ""
      );
    }
    if (coctailsStorage.deleteValue(removeInfoDrinck)) {
      alert("Напиток удален успешно");
    } else {
      alert("Такого напитка в базе данных нет");
    }
  }

  function getDrinckInfoAll() {
    let drinckAll = coctailsStorage.getKeys();
    if (drinckAll.length !== 0) {
      drinckAll = drinckAll.join(",");
      alert(drinckAll);
    } else {
      alert(`
      В базе данных напитков нет!
     Вы не внесли ни один напиток`);
    }
  }
})();
