"use strict";
class HashStorage {
  constructor() {
    this.coctailsStorage = {};
  }

  addValue(key, value) {
    this.coctailsStorage[key] = value;
  }

  getValue(key) {
    return this.coctailsStorage[key];
  }

  deleteValue(key) {
    if (key in this.coctailsStorage) {
      delete this.coctailsStorage[key];
      return true;
    } else {
      return false;
    }
  }

  getKeys() {
    return Object.keys(this.coctailsStorage);
  }
}
