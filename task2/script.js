/*
js-скрипте создаем две функции compress и uncompress, которые будут выполнять следующее:

Пример:
Вызов:
compress("a");
Результат:
Результат: “a”
Вызов:
compress("aaa");
Результат:
Результат: “a3”
Вызов:
compress("aabbb");
Результат:
Результат: “a2b3”
Вызов:
compress("aaabcc");
Результат:
Результат: “a3b1c2”
Вызов:
uncompress("a");
Результат:
Результат: “a”
Вызов:
uncompress("a5");
Результат:
Результат: “aaaaa”
Вызов:
uncompress("a2b3");
Результат:
Результат: “aabbb”
Вызов:
uncompress("a2b1c3");
Результат:
Результат: “aabccc”

*/

const compress = (str) => {
  if (str.length <= 1) {
    return str;
  }
  let obj = {};
  let newStr = "";
  for (let i = 0; i < str.length; i++) {
    const letter = str[i];
    if (!(letter in obj)) {
      obj[letter] = 1;
    } else {
      obj[letter]++;
    }
  }

  for (const elem in obj) {
    newStr += `${elem}${obj[elem]}`;
  }

  return newStr;
};

console.log(compress("aaabcc"));
console.log(compress("a"));
console.log(compress("aaa"));
console.log(compress("aabb"));

const uncompress = (str) => {
  if (str.length <= 1) {
    return str;
  }
  let result = "";
  for (let i = 0; i < str.length; i++) {
    result += str[i].repeat(+str[i + 1]);
  }
  return result;
};

console.log(uncompress("a"));
console.log(uncompress("a5"));
console.log(uncompress("a2b3"));
console.log(uncompress("a2b1c3"));

/*
 создаем функцию countVowelLetters, которая будет возвращать количество русских гласных букв в строке,
 которая будет являться аргументом функции. Не использовать switch, вложенные циклы, регулярные 
 выражения или 9-10 if для проверки… Можно использовать массивы или объекты для решения задачи, 
 найти оптимальное и эффективное из доступных решение.


*/
function countVowelLetters(str) {
  const lettersRus = [
    "а",
    "А",
    "о",
    "О",
    "и",
    "И",
    "е",
    "Э",
    "ё",
    "Ё",
    "э",
    "Э",
    "ы",
    "Ы",
    "у",
    "У",
    "ю",
    "Ю",
    "я",
    "Я",
  ];
  const lettersEn = [
    "A",
    "a",
    "E",
    "e",
    "I",
    "i",
    "O",
    "o",
    "U",
    "u",
    "Y",
    "y",
  ];

  const lettersAll = lettersRus.concat(lettersEn);

  // Первый способ

  /*
  const arr = str.split(" ").join("").split("");
  const result = arr.filter((elem) => {
    return lettersAll.indexOf(elem) !== -1;
  });
   return `Количество гласных = ${result.length}`;
 */

  // Второй способ, который гораздо быстреее

  let result = 0;
  for (let i = 0; i < str.length; i++) {
    if (lettersAll.indexOf(str[i]) !== -1) {
      result++;
    }
  }
  return `Количество гласных = ${result}`;
}

console.log(countVowelLetters("Пришла зима, запахло…"));
console.log(countVowelLetters("Ghbdtn, z r dfv bp Hjccbb"));
console.log(countVowelLetters("длинношеее"));

/*
Рекурсия:
Напишите программу на JavaScript, чтобы получить первые n чисел Фибоначчи.
Примечание: Последовательность Фибоначчи - это последовательность
 чисел: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34,... Каждое последующее число
 является суммой двух предыдущих.
*/

function isFinabonachi(n) {
  if (n === 1) {
    return [0, 1];
  } else {
    let arr = isFinabonachi(n - 1);
    arr.push(arr[arr.length - 1] + arr[arr.length - 2]);
    return arr.slice(0, n);
  }
}
console.log(isFinabonachi(9));
