"use strict";
/*
Напишите функцию sum, которая бы работала следующим образом:

sum(1)(2) == 3; // 1 + 2
sum(1)(2)(3) == 6; // 1 + 2 + 3
sum(5)(-1)(2) == 6
sum(6)(-1)(-2)(-3) == 0
sum(0)(1)(2)(3)(4)(5) == 15
*/

function sum(num) {
  let summ = num;

  function fun(b) {
    summ += b;
    return fun;
  }
  fun.toString = function () {
    return summ;
  };
  return fun;
}

//alert(sum(1)(2));
//alert(sum(5)(-1)(2));
//alert(sum(6)(-1)(-2)(-3));
//alert(sum(0)(1)(2)(3)(4)(5));
/*
Digital root is the recursive sum of all the digits in a number.

Given n, take the sum of the digits of n. If that value has more than one digit,
 continue reducing in this way until a single-digit number is produced. The input will be a non-negative integer.


*/

function digitalRoot(num) {
  const numbers = num.split("").join("+");
  let sum = 0;
  for (let i = 0; i < num.length; i++) {
    sum += Number(num[i]);
  }
  console.log(`${numbers}=${sum}`);
  if (sum > 9) {
    return digitalRoot(sum.toString());
  } else {
    return `${numbers}=${sum}`;
  }
}
//digitalRoot("1236554");
/*
You will be given an array of numbers. You have to sort the odd numbers in ascending order while leaving the even numbers at their original positions.

Examples
[7, 1]  =>  [1, 7]
[5, 8, 6, 3, 4]  =>  [3, 8, 6, 5, 4]
[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]  =>  [1, 8, 3, 6, 5, 4, 7, 2, 9, 0]

*/

function sortArray(arr) {
  for (var i = 0; i < arr.length - 1; i++) {
    if (arr[i] % 2 !== 0) {
      for (let j = i + 1; j < arr.length; j++) {
        if (arr[j] % 2 !== 0) {
          if (arr[i] > arr[j]) {
            let min = arr[j];
            arr[j] = arr[i];
            arr[i] = min;
          }
        }
      }
    }
  }
  return arr;
}
//console.log(sortArray([7, 1]));
//console.log(sortArray([5, 8, 6, 3, 4]));
//console.log(sortArray([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]));

/*
What is an anagram? Well, two words are anagrams of each other if they both contain the same letters. For example:

'abba' & 'baab' == true

'abba' & 'bbaa' == true

'abba' & 'abbba' == false

'abba' & 'abca' == false
Write a function that will find all the anagrams of a word from a list. You will be given two inputs a word and an array with words. You should return an array of all the anagrams or an empty array if there are none. For example:

anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']) => ['aabb', 'bbaa']

anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']) => ['carer', 'racer']

anagrams('laser', ['lazing', 'lazy',  'lacer']) => []

*/
function isAnagrams(word, arr) {
  let anagrams = [];
  for (const elem of arr) {
    if (compareWords(word, elem) && compareWords(elem, word)) {
      anagrams.push(elem);
    }
  }
  return anagrams;
}

function compareWords(word, elem) {
  let flag = true;
  if (word.length !== elem.length) {
    return false;
  } else {
    for (let i = 0; i < elem.length; i++) {
      if (word.indexOf(elem[i]) === -1) {
        return false;
      }
    }
  }
  return flag;
}

const arr1 = isAnagrams("racer", [
  "crazer",
  "carer",
  "racar",
  "caers",
  "racer",
]);
console.log(arr1);

const arr2 = isAnagrams("abba", ["aabb", "abcd", "bbaa", "dada"]); // ['aabb', 'bbaa']
console.log(arr2);

const arr3 = isAnagrams("laser", ["lazing", "lazy", "lacer"]); // []
console.log(arr3);
